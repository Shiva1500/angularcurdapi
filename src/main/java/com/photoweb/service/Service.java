package com.photoweb.service;

public interface Service {
	public String saveLoginDetails();
	public String validateLogin();
}
