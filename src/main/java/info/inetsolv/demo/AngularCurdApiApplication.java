package info.inetsolv.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "info.inetsolv")
public class AngularCurdApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AngularCurdApiApplication.class, args);
	}

}
