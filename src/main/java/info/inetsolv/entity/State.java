package info.inetsolv.entity;

import org.springframework.stereotype.Component;

@Component
public class State {
public Integer stateId;
public String state;
public Integer distId;
public Integer getStateId() {
	return stateId;
}
public void setStateId(Integer stateId) {
	this.stateId = stateId;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public Integer getDistId() {
	return distId;
}
public void setDistId(Integer distId) {
	this.distId = distId;
}


}
