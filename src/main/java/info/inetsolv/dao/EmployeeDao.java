package info.inetsolv.dao;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import info.inetsolv.entity.Employee;
import info.inetsolv.entity.State;
@Component
public class EmployeeDao {
	@Autowired
	private JdbcTemplate  jdbcTemplate;

	public List<Employee> getemployeeData() {
		String query = "select * from emp_tbl";
		return jdbcTemplate.query(query, (rs) ->{
			List<Employee> empList = new ArrayList<Employee>();
			while(rs.next()) {
				Employee employee = new Employee();
				employee.setEno(rs.getInt("eno"));
				employee.setName(rs.getString("name"));
				employee.setSalary(rs.getDouble("salary"));
				empList.add(employee);
			}
			return empList;
		});
			
	}
	

	public List<Employee> getemployeeDataById(Integer eno) {
		String query = "select * from emp_tbl where eno = "+eno;
		return jdbcTemplate.query(query, (rse) -> {
			List<Employee> list = new ArrayList<Employee>();
			while(rse.next()) {
				Employee emp  =new Employee();
				emp.setEno(rse.getInt("eno"));
				emp.setName(rse.getString("name"));
				emp.setSalary(rse.getDouble("salary"));
				list.add(emp);
			}
			return list;
		});
	}
	
	public int saveEmployeeData(Employee employee) {
		String query = "insert into emp_tbl values ("+employee.getEno()+",'"+employee.getName()+"',"+employee.getSalary() +")";
		return jdbcTemplate.update(query);
	}
	
	public List<State> getAllState(){
		String query = "select * from ent_state ";
		List<State> stateList = new ArrayList<State>();
		return jdbcTemplate.query(query, (rse) -> {
			while(rse.next()) {
				State state = new State();
				System.out.println("rse.getInt(\"stateid\")--> "+rse.getInt("stateid"));
				state.setStateId(rse.getInt("stateid"));
				state.setState(rse.getString("state"));
				state.setDistId(rse.getInt("distid"));
				stateList.add(state);
			}
			return stateList;
		});
	}
}
